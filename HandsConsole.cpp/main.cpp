﻿#include <windows.h>
#include <iostream>

#include "pxcsensemanager.h"
#include "pxcmetadata.h"
#include "service/pxcsessionservice.h"

#include "pxchandmodule.h"
#include "pxchanddata.h"
#include "pxchandconfiguration.h"


#include "Definitions.h"
#include "timer.h"
#include "PointData.h"

using namespace Intel::RealSense;
using namespace Intel::RealSense::Hand;

bool g_live = true;       // true - Working in live camera mode, false - sequence mode
bool g_gestures = false;   // Writing gesture data to console output
bool g_alerts = false;   // Writing alerts data to console output
bool g_default = true;      // Writing hand type to console output
bool g_stop = false;   // user closes application
bool g_Info = false;   // Skeleton Information
bool g_timer = false;    // Enable FPS output
PointData pointData;
std::wstring g_sequencePath;

PXCSession            *g_session;
PXCSenseManager         *g_senseManager;
PXCHandModule         *g_handModule;
PXCHandData            *g_handDataOutput;
PXCHandConfiguration   *g_handConfiguration;


void releaseAll();

BOOL CtrlHandler(DWORD fdwCtrlType)
{
	switch (fdwCtrlType)
	{
		// Handle the CTRL-C signal. 
	case CTRL_C_EVENT:

		// confirm that the user wants to exit. 
	case CTRL_CLOSE_EVENT:
		g_stop = true;
		Sleep(1000);
		releaseAll();
		return(TRUE);

	default:
		return FALSE;
	}
}

//typedef struct 

void main(int argc, const char* argv[])
{
	// Measuring FPS
	FPSTimer timer;
	bool   direct;

	Definitions::appName = argv[0];

	SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE);
	if (argc < 2)
	{
		g_live = true;
		std::printf("Tip: Run HandsConsole.exe -help for additional parameters\n");
	}

	if (argc == 2)
	{
		if (strcmp(argv[1], "-help") == 0)
		{
			Definitions::WriteHelpMessage();
			return;
		}
	}

	// Setup
	g_session = PXCSession::CreateInstance();
	if (!g_session)
	{
		std::printf("Failed Creating PXCSession\n");
		return;
	}

	g_senseManager = g_session->CreateSenseManager();
	if (!g_senseManager)
	{
		releaseAll();
		std::printf("Failed Creating PXCSenseManager\n");
		return;
	}

	int moduleCount = 0;

	// Iterating input parameters
	for (int i = 1; i<argc; i++)
	{
		if (strcmp(argv[i], "-live") == 0)
		{
			g_live = true;
		}

		if (strcmp(argv[i], "-seq") == 0)
		{
			if (argc == i + 1)
			{
				releaseAll();
				std::printf("Error: Sequence path is missing\n");
				return;
			}
			g_live = false;
			std::string tmp(argv[i + 1]);
			i++;
			g_sequencePath.clear();
			g_sequencePath.assign(tmp.begin(), tmp.end());
			continue;
		}

		if (strcmp(argv[i], "-fps") == 0)
		{
			g_timer = true;
		}

		if (strcmp(argv[i], "-gestures") == 0)
		{
			g_gestures = true;
		}

		if (strcmp(argv[i], "-alerts") == 0)
		{
			g_alerts = true;
		}

		if (strcmp(argv[i], "-info") == 0)
		{
			g_Info = true;
		}

	}

	if (g_senseManager->EnableHand(0) != pxcStatus::PXC_STATUS_NO_ERROR)
	{
		releaseAll();
		std::printf("Failed Enabling Hand Module\n");
		return;
	}

	g_handModule = g_senseManager->QueryHand();
	if (!g_handModule)
	{
		releaseAll();
		std::printf("Failed Creating PXCHandModule\n");
		return;
	}

	g_handDataOutput = g_handModule->CreateOutput();
	if (!g_handDataOutput)
	{
		releaseAll();
		std::printf("Failed Creating PXCHandData\n");
		return;
	}

	g_handConfiguration = g_handModule->CreateActiveConfiguration();
	if (!g_handConfiguration)
	{
		releaseAll();
		std::printf("Failed Creating PXCHandConfiguration\n");
		return;
	}

	g_handConfiguration->SetTrackingMode(HandData::TRACKING_MODE_FULL_HAND);

	if (g_gestures)
	{
		std::printf("-Gestures Are Enabled-\n");
		g_handConfiguration->EnableAllGestures();
	}

	if (g_alerts)
	{
		std::printf("-Alerts Are Enabled-\n");
		g_handConfiguration->EnableAllAlerts();
	}

	// Apply configuration setup
	g_handConfiguration->ApplyChanges();

	// run sequences as fast as possible
	if (!g_live)
	{
		if (g_senseManager->QueryCaptureManager()->SetFileName(g_sequencePath.c_str(), false) != PXC_STATUS_NO_ERROR)
		{
			releaseAll();
			std::printf("Error: Invalid Sequence/ Sequence path\n");
			return;
		}
		g_senseManager->QueryCaptureManager()->SetRealtime(false);
	}

	pxcI32 numOfHands = 0;

	// First Initializing the sense manager
	pxcStatus initStatus = g_senseManager->Init();
	if (initStatus == PXC_STATUS_NO_ERROR)
	{
		std::printf("\nPXCSenseManager Initializing OK\n========================\n");


		// Acquiring frames from input device
		while (g_senseManager->AcquireFrame(true) == PXC_STATUS_NO_ERROR && !g_stop)
		{
			// Get current hand outputs
			if (g_handDataOutput->Update() == PXC_STATUS_NO_ERROR)
			{

				// Display alerts
				if (g_alerts)
				{
					PXCHandData::AlertData alertData;
					for (int i = 0; i < g_handDataOutput->QueryFiredAlertsNumber(); ++i)
					{
						if (g_handDataOutput->QueryFiredAlertData(i, alertData) == PXC_STATUS_NO_ERROR)
						{
							std::printf("%s was fired at frame %d \n", Definitions::AlertToString(alertData.label).c_str(), alertData.frameNumber);
						}
					}
				}

				// Display gestures
				if (g_gestures)
				{
					PXCHandData::GestureData gestureData;
					int check = g_handDataOutput->QueryFiredGesturesNumber();
					for (int i = 0; i < check; ++i)
					{
						if (g_handDataOutput->QueryFiredGestureData(i, gestureData) == PXC_STATUS_NO_ERROR)
						{
							std::wprintf(L"%s, Gesture: %s was fired at frame %d \n", Definitions::GestureStateToString(gestureData.state), gestureData.name, gestureData.frameNumber);
						}
					}
				}

				// Display joints
				if (g_Info)
				{
					PXCHandData::IHand *hand;
					PXCHandData::JointData jointData;
					for (int i = 0; i < g_handDataOutput->QueryNumberOfHands(); ++i)
					{
						g_handDataOutput->QueryHandData(PXCHandData::ACCESS_ORDER_BY_TIME, i, hand);
						std::string handSide = "Unknown Hand";
						handSide = hand->QueryBodySide() == PXCHandData::BODY_SIDE_LEFT ? "Left Hand" : "Right Hand";

						std::printf("%s\n==============\n", handSide.c_str());
						for (int j = 0; j<22; j++)
						{
							if (hand->QueryTrackedJoint((PXCHandData::JointType)j, jointData) == PXC_STATUS_NO_ERROR)
							{
								std::printf("     %s)\tX: %f, Y: %f, Z: %f \n", Definitions::JointToString((PXCHandData::JointType)j).c_str(), jointData.positionWorld.x, jointData.positionWorld.y, jointData.positionWorld.z);
							}
						}
					}
				}
			}



			// Display number of hands
			if (g_default)
			{
				PointData pointData2;//4손가락의 좌표를 저장할 객체 center thumb middle 

				if (numOfHands != g_handDataOutput->QueryNumberOfHands())
				{
					numOfHands = g_handDataOutput->QueryNumberOfHands();
					std::printf("Number of hands: %d\n", numOfHands);
				}
				PXCHandData::IHand *hand;
				PXCHandData::JointData jointData;
				
				//if 손이안찍혔을때 배열 초기화
				//배열이 5개다 차면 계산하고 초기화, 1번 배열값과 2번 배열값이 같으면 1번에 다시 쓴다.
				//배열4개 생성해서 4개 값 and
				for (int i = 0; i < g_handDataOutput->QueryNumberOfHands(); ++i)
				{
					g_handDataOutput->QueryHandData(PXCHandData::ACCESS_ORDER_BY_TIME, i, hand);
					std::string handSide = "Unknown Hand";
					handSide = hand->QueryBodySide() == PXCHandData::BODY_SIDE_LEFT ? "Left Hand" : "Right Hand";
					system("cls");
					std::printf("%s\n==============\n", handSide.c_str());
					//timer,배열값
					for (int j = 0; j < 22; j++)
					{
						if (j == 1 || j == 5 || j == 13 || j == 21)
						{
							if (hand->QueryTrackedJoint((PXCHandData::JointType)j, jointData) == PXC_STATUS_NO_ERROR)
							{
								std::printf("     %s)\tX: %f, Y: %f, Z: %f \n", Definitions::JointToString((PXCHandData::JointType)j).c_str(), jointData.positionWorld.x, jointData.positionWorld.y, jointData.positionWorld.z);
								/*if (-0.05 < jointData.positionWorld.x && jointData.positionWorld.x < 0.05 && -0.05<jointData.positionWorld.y && jointData.positionWorld.y<0.05)
									pointData.savePoint(jointData.positionWorld.x, jointData.positionWorld.y, j);*/
							}
						}
						pointData2.savePoint(jointData.positionWorld.x, jointData.positionWorld.y, j);
						
					}
					
					//printf("%f %f", pointData.point[0], pointData.point[1]);
					printf("%d", pointData.state);
					int j = 1;
					if (hand->QueryTrackedJoint((PXCHandData::JointType)j, jointData) == PXC_STATUS_NO_ERROR) {
						//pointData2.State();
						if (pointData.state == 2) {//편 손의 상태일때
							if (pointData.point[0] != 5.0 && pointData.point[1] != 5.0) {
								//초기값이 아니면 기능 수행
								if (jointData.positionWorld.x > 0.1 && -0.5 < jointData.positionWorld.y&& jointData.positionWorld.y < 0.5) {
									printf("LEFT");
									keybd_event(VK_LEFT, 0, 0, 0);
									keybd_event(VK_LEFT, 0, KEYEVENTF_KEYUP, 0);
									pointData.pointInit();
									Sleep(2000);
									//편 손의 상태에서 화면의 중심에서 좌측으로 이동시 좌표배열에 저장해놓은 센터 값과 현재의 좌표값을 비교하여
									//LEFT기능 수행
								}
								else if (jointData.positionWorld.x < -0.1 && -0.05 < jointData.positionWorld.y&& jointData.positionWorld.y < 0.05) {
									printf("RIGHT");
									keybd_event(VK_RIGHT, 0, 0, 0);
									keybd_event(VK_RIGHT, 0, KEYEVENTF_KEYUP, 0);
									pointData.pointInit();
									Sleep(2000);
									//RIGHT기능 수행
								}
								else if (jointData.positionWorld.y < -0.1 && -0.05 < jointData.positionWorld.x&& jointData.positionWorld.x < 0.05) {
									printf("down");
									keybd_event(VK_DOWN, 0, 0, 0);
									keybd_event(VK_DOWN, 0, KEYEVENTF_KEYUP, 0);
									pointData.pointInit();
									Sleep(2000);
									//DOWN기능 수행
								}
								else if (jointData.positionWorld.y > 0.1 && -0.05 < jointData.positionWorld.x&& jointData.positionWorld.x < 0.05) {
									printf("up");
									keybd_event(VK_UP, 0, 0, 0);
									keybd_event(VK_UP, 0, KEYEVENTF_KEYUP, 0);
									pointData.pointInit();
									Sleep(2000);
									//UP기능 수행
								}
								else if (-0.05 < jointData.positionWorld.x && jointData.positionWorld.x < 0.05 && -0.05 < jointData.positionWorld.y && jointData.positionWorld.y < 0.05 && pointData2.point[4]<0.05&&pointData2.point[4]>-0.05&&pointData2.point[5]<0.05&&pointData2.point[5]>-0.05) {
									if (pointData.sp == 1) {
										printf("pause");
										keybd_event(VK_SPACE, 0, 0, 0);
										keybd_event(VK_SPACE, 0, KEYEVENTF_KEYUP, 0);
										pointData.sp = 2;
									}
									else if (pointData.sp == 2) {
										printf("start");
										keybd_event(VK_SPACE, 0, 0, 0);
										keybd_event(VK_SPACE, 0, KEYEVENTF_KEYUP, 0);
										pointData.sp = 1;
									}
									pointData.pointInit();
									Sleep(2000);
									//편손의 상태에서 중지가 중앙의 인식범위 내로 이동했을때 PAUSE기능 수행
								}
							}
							else if (-0.08 < jointData.positionWorld.x && jointData.positionWorld.x < 0.08 && -0.08 < jointData.positionWorld.y && jointData.positionWorld.y < 0.08) {
								pointData.savePoint(jointData.positionWorld.x, jointData.positionWorld.y, j);
								Sleep(800);//초기값일때 (0,0)에서 일정범위 내에 센터가있을 시에 그 점을 좌표 배열에 저장 

							}
						}
						else if (pointData.state == 1) {//한 손가락만 핀 상태
							
							if (pointData.point[0] != 5.0 && pointData.point[1] != 5.0) {

								if (jointData.positionWorld.x > 0.05 && -0.05 < jointData.positionWorld.y&& jointData.positionWorld.y < 0.05) {
									printf("OneFingerLEFT");
									keybd_event(VK_PRIOR, 0, 0, 0);
									keybd_event(VK_PRIOR, 0, KEYEVENTF_KEYUP, 0);
									pointData.pointInit();
									Sleep(2000);
									//한 손가락만 핀 상태에서 센터가 좌로 이동시 기능 수행 
								}
								else if (jointData.positionWorld.x < -0.05 && -0.05 < jointData.positionWorld.y&& jointData.positionWorld.y < 0.05) {
									printf("OneFingerRIGHT");
									keybd_event(VK_NEXT, 0, 0, 0);
									keybd_event(VK_NEXT, 0, KEYEVENTF_KEYUP, 0);
									pointData.pointInit();
									Sleep(2000);
									//한 손가락만 핀 상태에서 센터가 우로 이동시 기능 수행 
								}
								//else if (-0.05 < jointData.positionWorld.x && jointData.positionWorld.x < 0.05 && -0.05 < jointData.positionWorld.y && jointData.positionWorld.y < 0.05&&pointData2.point[5]>0.12) {
								//	printf("start");
								//	pointData.pointInit();
								//	Sleep(1000);
								//	//편손의 상태에서 중지가 중앙의 인식범위 qkR로 이동했을때 기능 수행
								//}
							}
							else if (-0.08 < jointData.positionWorld.x && jointData.positionWorld.x < 0.08 && -0.08 < jointData.positionWorld.y && jointData.positionWorld.y < 0.08) {
								pointData.savePoint(jointData.positionWorld.x, jointData.positionWorld.y, j);

								Sleep(800);

							}
						}
						pointData2.State();//4손가락의 좌표로 현재 손의 상태 인식
						pointData.state = pointData2.state;
					}
					
					/*printf("%f %f", pointData.point[0], pointData.point[1]);
					if(hand->QueryTrackedJoint((PXCHandData::JointType)j, jointData) == PXC_STATUS_NO_ERROR) {
						
						if (pointData.point[0] != 5.0 && pointData.point[1] != 5.0) {
							
							if (jointData.positionWorld.x >0.05 && -0.1<jointData.positionWorld.y&& jointData.positionWorld.y<0.1) {
								printf("LEFT");
								pointData.point[0] = 5.0;
								pointData.point[1] = 5.0;
								Sleep(1000);
							}
							else if (jointData.positionWorld.x <-0.05 && -0.1<jointData.positionWorld.y&& jointData.positionWorld.y<0.1) {
								printf("RIGHT");
								pointData.point[0] = 5.0;
								pointData.point[1] = 5.0;
								Sleep(1000);
							}
							else if (jointData.positionWorld.y <-0.05 && -0.1<jointData.positionWorld.x&& jointData.positionWorld.x<0.1) {
								printf("down");
								pointData.point[0] = 5.0;
								pointData.point[1] = 5.0;
								Sleep(1000);
							}
							else if (jointData.positionWorld.y >0.05 && -0.1<jointData.positionWorld.x&& jointData.positionWorld.x<0.1) {
								printf("up");
								pointData.point[0] = 5.0;
								pointData.point[1] = 5.0;
								Sleep(1000);
							}
						}
					else if(-0.05 < jointData.positionWorld.x && jointData.positionWorld.x < 0.05 && -0.05<jointData.positionWorld.y && jointData.positionWorld.y<0.05) {
							pointData.savePoint(jointData.positionWorld.x, jointData.positionWorld.y,j);
							
							Sleep(300);

						}
					}*/
				
			
				}
			}

			if (g_timer)
			{
				timer.Tick();
			}

			g_senseManager->ReleaseFrame();
		} // end while acquire frame
	} // end if Init

	else
	{
		releaseAll();
		std::printf("Failed Initializing PXCSenseManager\n");
		return;
	}


	releaseAll();
}

void releaseAll()
{
	if (g_handConfiguration)
	{
		g_handConfiguration->Release();
		g_handConfiguration = NULL;
	}
	if (g_handDataOutput)
	{
		g_handDataOutput->Release();
		g_handDataOutput = NULL;
	}
	if (g_senseManager)
	{
		g_senseManager->Close();
		g_senseManager->Release();
		g_senseManager = NULL;
	}
	if (g_session)
	{
		g_session->Release();
		g_session = NULL;
	}
}
