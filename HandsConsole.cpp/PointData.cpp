#include <windows.h>
#include <iostream>

#include "pxcsensemanager.h"
#include "pxcmetadata.h"
#include "service/pxcsessionservice.h"

#include "pxchandmodule.h"
#include "pxchanddata.h"
#include "pxchandconfiguration.h"


#include "Definitions.h"
#include "timer.h"
#include "PointData.h"


void PointData::savePoint(float x, float y,int a) {
	if (a == 1) {
		point[0] = x;
		point[1] = y;
	}
	else if (a == 5) {
		point[2] = x;
		point[3] = y;
	}
	else if (a == 13) {
		point[4] = x;
		point[5] = y;
	}
	else if (a == 21) {
		point[6] = x;
		point[7] = y;
	}
}

void PointData::State() {
	
		if ((point[4]-point[0])<0.05&& (point[4] - point[0])>-0.05&&(point[5] - point[1])<0.05&& (point[5] - point[1])>-0.05) {

			state = 1;
		}
		else
			state = 2;
	
}

void PointData::pointInit() {
	for (int i = 0; i < 8; i++) {
		if (i == 1 || i == 0)
			point[i] = 5.0;
		else
			point[i] = 10.0;
	}
}